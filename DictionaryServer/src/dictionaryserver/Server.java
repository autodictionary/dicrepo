
package dictionaryserver;

import java.util.List;


@javax.jws.WebService
public class Server {

    @javax.jws.WebMethod
    public boolean SaveDic(String dicName, List<String> dic) {
       try
       {
           ServerFileWorker.writeDic(dicName, dic);
       }
       catch (Exception ex)
       {
           return false;
       }
       return true;
    }
    
     @javax.jws.WebMethod
    public List<String> GetDicNames() {
       try
       {
           return ServerFileWorker.getDicNames();
       }
       catch (Exception ex)
       {
           return null;
       }
    }
    
       @javax.jws.WebMethod
    public List<String> GetDic(String name) {
       try
       {
           return ServerFileWorker.read(name);
       }
       catch (Exception ex)
       {
           return null;
       }
    }
}

