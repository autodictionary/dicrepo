/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import static dictionary.Dictionary.dictionary;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.openqa.selenium.Platform;

/**
 *
 * @author Евгений Пиляев
 */
public class FXMLDocumentController implements Initializable {

    ObservableList<String> items = FXCollections.observableArrayList();
    ObservableList<String> dictionaries = FXCollections.observableArrayList(
            "All", "BSE", "Ozhegov", "Vedu", "Slovopedia", "SumInUa");

    @FXML
    private Label label;

    @FXML
    private Label label1;
    @FXML
    private Label ExcludeLabel;
    @FXML
    private Label ExcludeFromLabel;
    @FXML
    private Label ExcludeToLabel;

    @FXML
    private Label infoLabel;

    @FXML
    private Label SettingsLabel;

    @FXML
    private Label DefSplLabel;

    @FXML
    private Label PosLabel;

    @FXML
    private TextArea RedactionArea;

    @FXML
    private Button SearchButton;

    @FXML
    private Button SaveButton;

    @FXML
    private ListView<String> listView;

    @FXML
    private ChoiceBox cb;

    @FXML
    private CheckBox directCheckBox;

    @FXML
    private CheckBox externalCheckBox;

    @FXML
    private TextField splitterTextField;

    @FXML
    private TextField defPosInSplittedTextField;
    
    @FXML
    private TextField ExcludeFromTextField;
    
    @FXML
    private TextField ExcludeToTextField;

    @FXML
    private void searchButtonPushed(ActionEvent event) throws FileNotFoundException {
        String term = listView.getSelectionModel().getSelectedItem();
        int termPos = listView.getSelectionModel().getSelectedIndex();
        if (!term.contains("(already found)") && !externalCheckBox.isSelected()) {
            externalCheckBox.fire();
        }
        term = term.replace("  (already found)", "");

        String dictionaryName = cb.valueProperty().getValue().toString();

         for (int i = 0; i < dictionary.sr.size(); i++) {
            if (dictionary.sr.get(i).dicName == dictionaryName) {
                dictionary.sr.get(i).splitter = splitterTextField.getText();
                
            dictionary.sr.get(i).defPos = new ArrayList<>();
            if (defPosInSplittedTextField.getText().length() > 0) {
                String[] defPosText = defPosInSplittedTextField.getText().split(",");
                for (String pos : defPosText) {
                    dictionary.sr.get(i).defPos.add(Integer.parseInt(pos));
                }
            }
            dictionary.sr.get(i).excludeFrom = ExcludeFromTextField.getText();
            dictionary.sr.get(i).excludeTo = ExcludeToTextField.getText();
                break;
            }
        }
        
        ExitSearch determination = dictionary.searchProcessing(termPos, dictionaryName, externalCheckBox.isSelected());

        switch (determination.state) {
            case error:
                RedactionArea.setText(term);
                infoLabel.setText("Error in search processing!");
                break;
            case not_found:
                RedactionArea.setText(term);
                infoLabel.setText("Haven't found definition!");
                break;
            case initial:
                RedactionArea.setText(determination.definition);
                infoLabel.setText("Found in initial vocabulary");
                break;
            case external:
                RedactionArea.setText(determination.definition);
                infoLabel.setText("Found in external vocabulary");
                break;
            case online:
                RedactionArea.setText(determination.definition);
                infoLabel.setText("Found in online vocabulary");
                break;
        }

        activateAreas();

    }

    void activateAreas() {

        RedactionArea.setDisable(false);
        SaveButton.setDisable(false);
    }

    @FXML
    private void saveButtonPushed(ActionEvent event) throws FileNotFoundException {

        String definition = RedactionArea.getText();
        if (definition.length() > 8) {
            int res = Dictionary.SaveWord(definition);
            if (res == 0) {
                RedactionArea.setText("");
                RedactionArea.setDisable(true);
                SaveButton.setDisable(true);
                infoLabel.setText("Successfully saved");
            } else {
                if (res == 1) {
                    infoLabel.setText("This definition already exists!");
                }
                if (res == 2) {
                    infoLabel.setText("Rewrited");
                }
            }
        }
        String cur = listView.getSelectionModel().getSelectedItem();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i) == cur && !items.get(i).contains("  (already found)")) {
                items.set(i, items.get(i) +  "  (already found)");
            }
        }
        listView.setItems(items);
    }

    @FXML
    private void directTicked() {
        Dictionary.isDirect = directCheckBox.selectedProperty().get();
    }

    @FXML
    private void goToServerPanel() throws IOException {
        dictionary.goToServerPanel();
    }

    void ChangedDictionary(ObservableValue ov, Number value, Number newValue) {

        disableSettings();
        int i = value.intValue() - 1;
        if (i >= 0 && i < 2) {

            dictionary.sr.get(i).splitter = splitterTextField.getText();
            dictionary.sr.get(i).defPos = new ArrayList<>();
            if (defPosInSplittedTextField.getText().length() > 0) {
                String[] defPosText = defPosInSplittedTextField.getText().split(",");
                for (String pos : defPosText) {

                    dictionary.sr.get(i).defPos.add(Integer.parseInt(pos));
                }
            }
            dictionary.sr.get(i).excludeFrom = ExcludeFromTextField.getText();
            dictionary.sr.get(i).excludeTo = ExcludeToTextField.getText();
        }

        i = newValue.intValue() - 1;

        if (i >= 0 && i < 2) {
            enableSettings();
            defPosInSplittedTextField.setText("");
            for (int p = 0; p < dictionary.sr.get(i).defPos.size(); p++) {
                if (p == dictionary.sr.get(i).defPos.size() - 1) {
                    defPosInSplittedTextField.setText(defPosInSplittedTextField.textProperty().getValue() + dictionary.sr.get(i).defPos.get(p));
                } else {
                    defPosInSplittedTextField.setText(defPosInSplittedTextField.textProperty().getValue() + dictionary.sr.get(i).defPos.get(p) + ",");
                }
            }
            defPosInSplittedTextField.deletePreviousChar();
            splitterTextField.setText(dictionary.sr.get(i).splitter);
            ExcludeFromTextField.setText(dictionary.sr.get(i).excludeFrom);
            ExcludeToTextField.setText(dictionary.sr.get(i).excludeTo);
        }

    }

    void enableSettings() {
        SettingsLabel.setVisible(true);
        DefSplLabel.setVisible(true);
        PosLabel.setVisible(true);
         ExcludeFromLabel.setVisible(true);
        ExcludeLabel.setVisible(true);
        ExcludeToLabel.setVisible(true);
        defPosInSplittedTextField.setVisible(true);
        splitterTextField.setVisible(true);
         ExcludeFromTextField.setVisible(true);
        ExcludeToTextField.setVisible(true);
    }

    void disableSettings() {
        SettingsLabel.setVisible(false);
        DefSplLabel.setVisible(false);
        PosLabel.setVisible(false);
        ExcludeFromLabel.setVisible(false);
        ExcludeLabel.setVisible(false);
        ExcludeToLabel.setVisible(false);
        defPosInSplittedTextField.setVisible(false);
        splitterTextField.setVisible(false);
        ExcludeFromTextField.setVisible(false);
        ExcludeToTextField.setVisible(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        SaveButton.setDisable(true);
        for (int i = 0; i < items.size(); i++) {
            if (Dictionary.InitialSearch(items.get(i)).state == SearchState.initial) {
                items.set(i, items.get(i) + "  (already found)");
            }
        }
        
        for(Term term:Dictionary.terms){
            items.add(term.GetFullTerm());
        }
        listView.setItems(items);

        cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                ChangedDictionary(observable, oldValue, newValue);
            }
        });

        cb.setItems(dictionaries);
        cb.setValue("All");
        
         
    }

}
