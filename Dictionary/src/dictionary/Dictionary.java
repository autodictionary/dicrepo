/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;
//testGit

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Ordering;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.openqa.selenium.WebDriver;

/**
 *
 * @author Евгений Пиляев
 */
public class Dictionary extends Application {

    Stage _stage;
    public static Dictionary dictionary;

    static ArrayList<ArrayList<String>> dictionaries;
    static ArrayList<String> BSE;
    static ArrayList<String> Ozhegov;
    static ArrayList<String> InitVoc;

    public ArrayList<SearchRule> sr;
    static WebDriver driver;

    //Точный поиск заданного слова (не может быть частью другого слова)
    public static boolean isDirect;

    public static Term[] terms = new Term[]{
        new Term(new TermPart[]{new TermPart("годовые", "годов"),
            new TermPart("эксплуатационные", "эксплуатационн"),
            new TermPart("расходы", "расход")}),
        new Term(new TermPart[]{new TermPart("задел", "задел")}),
        new Term(new TermPart[]{new TermPart("затраты", "затрат")}),
        new Term(new TermPart[]{new TermPart("затраты", "затрат"),
            new TermPart("единовременные", "единовременн")}),
        new Term(new TermPart[]{new TermPart("затраты", "затрат"),
            new TermPart("на", "на", true),
            new TermPart("материалы", "материал")}),
        new Term(new TermPart[]{new TermPart("зарплата", "зарплата")}),
        new Term(new TermPart[]{new TermPart("зарплата", "зарплата"),
            new TermPart("основная", "основн")}),
        new Term(new TermPart[]{new TermPart("издержки", "издержк")}),
        new Term(new TermPart[]{new TermPart("межоперационный", "межоперационн"),
            new TermPart("задел", "задел")})
    };

    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

        Scene scene = new Scene(root);
        sr = new ArrayList<>();
        sr.add(new SearchRule("BSE", " ", -1));
        sr.add(new SearchRule("Ozhegov", "\\|", 5));
        dictionaries = new ArrayList<>();
        dictionaries.add(BSE);
        dictionaries.add(Ozhegov);
        stage.setScene(scene);
        stage.show();
        stage.titleProperty().set("Dictionary");
        _stage = stage;
        dictionary = this;
    }

    public void goToServerPanel() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("ServerPanel.fxml"));
        Scene scene = new Scene(root);
        _stage.setScene(scene);
        _stage.show();
    }

    public void goToMainPanel() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        _stage.setScene(scene);
        _stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        launch(args);

    }

    public ExitSearch searchProcessing(int termPos, String dictionary, boolean useExternal) {

        if (!useExternal) {
            return InitialSearch(terms[termPos].GetFullTerm());
        }

        switch (dictionary) {
            case "Vedu":
                return WebDictionary.findWordInWebVedu(terms[termPos].GetFullTerm());
            case "Slovopedia":
                return WebDictionary.findWordInWebSlovopedia(terms[termPos].GetFullTerm());
            case "SumInUa":
                return WebDictionary.findWordInWebSumIn(terms[termPos].GetFullTerm());
            default:
                return SearchWordInExternal(terms[termPos], dictionary);

        }
    }

    public String findWordInDic(String word, int dicPos) throws FileNotFoundException {
        String exit = new String();
        SearchRule rule = sr.get(dicPos);

        if (BSE == null) {
            BSE = FileWorker.read("BSE.txt");
            dictionaries.set(0, BSE);
        }
        if (Ozhegov == null) {
            Ozhegov = FileWorker.read("Ozhegov.txt");
            dictionaries.set(1, Ozhegov);
        }

        for (String definition : dictionaries.get(dicPos)) {
            String s = definition.split(rule.splitter)[0];
            if ((!isDirect && s.toLowerCase().startsWith(word.toLowerCase()) || (isDirect && s.equalsIgnoreCase(word)))) {

                if (rule.defPos.size() == 0 || rule.defPos.get(0) < 0) {
                    exit += definition + "\n\n";
                } else {

                    String[] result = definition.split(rule.splitter);
//                            char[] arr = result[0].toCharArray();
//                            arr[0] -= 32;
//                            result[0] = new String(arr);
                    exit += result[0] + " - ";
                    for (int i = 0; i < rule.defPos.size(); i++) {
                        if (i == 0) {
                            exit += result[rule.defPos.get(i)];
                        } else {
                            exit += ", " + result[rule.defPos.get(i)];
                        }

                    }
                    exit += "\n\n";
                }

            }
        }
        if (rule.excludeFrom != null && rule.excludeTo != null && rule.excludeFrom.length() > 0 && rule.excludeTo.length() > 0) {
            exit = exit.replaceAll("\\" + rule.excludeFrom + ".*?\\" + rule.excludeTo, "");
        }
        return exit;
    }

    public ExitSearch SearchWordInExternal(Term term, String dictionary) {
        try {
            String definition = new String();
            String fullTerm = term.GetFullTerm();
            switch (dictionary) {
                case "All":
                    definition = findWordInDic(fullTerm, 0) + findWordInDic(fullTerm, 1);

                    for (String norm : term.GetNormalParts()) {
                        definition += findWordInDic(norm, 0) + findWordInDic(norm, 1);
                    }
                    if (term.termParts.length > 1) {
                        for (String comb : term.GetCombinations()) {
                            definition += findWordInDic(comb, 0) + findWordInDic(comb, 1);
                        }
                    }
                    break;

                case "BSE":
                    definition = findWordInDic(fullTerm, 0);
                    for (String norm : term.GetNormalParts()) {
                        definition += findWordInDic(norm, 0);
                    }
                    if (term.termParts.length > 1) {
                        for (String comb : term.GetCombinations()) {
                            definition += findWordInDic(comb, 0);
                        }
                    }
                    break;

                case "Ozhegov":
                    definition = findWordInDic(fullTerm, 1);
                    for (String norm : term.GetNormalParts()) {
                        definition += findWordInDic(norm, 1);
                    }
                    if (term.termParts.length > 1) {
                        for (String comb : term.GetCombinations()) {
                            definition += findWordInDic(comb, 1);
                        }
                    }
                    break;
            }

            if (definition.length() > 0) {
                //filter and sort definitions
                definition = FilterAndSortDefinitions(definition);
                return new ExitSearch(SearchState.external, definition);
            } else {
                return new ExitSearch(SearchState.not_found, null);
            }
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
            return new ExitSearch(SearchState.error, null);
        }
    }

    int maxDefs = 10;

    private String FilterAndSortDefinitions(String defs) {
        String[] definitions = defs.split("\n\n");

        Map<String, Integer> map = new HashMap<String, Integer>();
        //init map
        for (String def : definitions) {
            map.put(def, 0);
            for (Term term : terms) {
                if (def.toLowerCase().contains(term.GetFullTerm().toLowerCase())) {
                    Integer newValue = map.get(def);
                    newValue++;
                    map.replace(def, newValue);
                }
                for (TermPart tp : term.termParts) {
                    if (!tp.ignored && def.toLowerCase().contains(tp.normalized.toLowerCase())) {
                        Integer newValue = map.get(def);
                        newValue++;
                        map.replace(def, newValue);
                    }
                }
            }
        }

        ValueComparator bvc = new ValueComparator(map);
        TreeMap<String, Integer> sortedMap = new TreeMap<String, Integer>(bvc);
        sortedMap.putAll(map);
        String ret = "";
        int keysCnt = sortedMap.keySet().size();
        if (keysCnt <= maxDefs) {
            for (int i = 0; i < keysCnt; i++) {
                ret += sortedMap.keySet().toArray()[i] + "\n\n";
            }
        } else {
            for (int i = 0; i < maxDefs; i++) {
                ret += sortedMap.keySet().toArray()[i] + "\n\n";
            }
        }

        return ret;
    }

    public static ExitSearch InitialSearch(String word) {
        try {
            if (InitVoc == null) {
                InitVoc = FileWorker.read("InitialVoc.txt");
            }
            word = word.toLowerCase();
            String definition = "";
            //1040=А 1071=Я A=65 Z=90
            if (InitVoc != null) {
                for (String def : InitVoc) {
                    String s = def;
                    s = s.toLowerCase();
                    if (s.startsWith(word)) {
                        definition += def;
                    }
                }
            }

            if (definition.length() > 0) {
                return new ExitSearch(SearchState.initial, definition);
            } else {
                return new ExitSearch(SearchState.not_found, null);
            }
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
            return new ExitSearch(SearchState.error, null);
        }
    }

    public static int SaveWord(String definition) throws FileNotFoundException {

        InitVoc = FileWorker.read("InitialVoc.txt");
        String tmp = definition.toLowerCase();
        if (InitVoc == null) {
            FileWorker.write("InitialVoc.txt", definition);
            return 0;
        } else {
            for (int i = 0; i < InitVoc.size(); i++) {
                String s = InitVoc.get(i).toLowerCase();
                if (s.compareTo(tmp) == 0) {
                    System.out.println("This definition already exists.");
                    return 1;
                }
                String word = definition.split(" ")[0];
                word = word.toLowerCase();
                if (s.startsWith(word)) {
                    InitVoc.set(i, definition);
                    i++;

                    while (i < InitVoc.size() && InitVoc.get(i).toLowerCase().startsWith(word)) {
                        InitVoc.remove(i);
                    }
                    FileWorker.rewriteDic("InitialVoc.txt", InitVoc);
                    return 2;
                }
            }
            InitVoc.add(definition);
            InitVoc.sort(String.CASE_INSENSITIVE_ORDER);
            FileWorker.rewriteDic("InitialVoc.txt", InitVoc);
            return 0;
        }
    }
}
