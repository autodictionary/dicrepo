/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import static dictionary.Dictionary.dictionary;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

/**
 *
 * @author Евгений Пиляев
 */
public class ServerPanelController implements Initializable {

    @FXML
    private ListView<String> serverDicsListView;

    @FXML
    private ListView<String> internalDicsListView;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        serverDicsListView.setItems(FXCollections.observableArrayList(Client.getDicNamesFromServer()));
        internalDicsListView.setItems(FXCollections.observableArrayList(FileWorker.getDicNames()));
    }

    @FXML
    private void goToServerPanel() throws IOException {
        dictionary.goToMainPanel();
    }

    @FXML
    private void pushDicToServer() throws IOException {
        String dic = internalDicsListView.getSelectionModel().getSelectedItem();
        if (dic != null) {
            Client.saveDicOnServer(dic, FileWorker.read(dic));
            serverDicsListView.setItems(FXCollections.observableArrayList(Client.getDicNamesFromServer()));
        }
    }

    @FXML
    private void getDicFromServer() throws IOException {
        String dic = serverDicsListView.getSelectionModel().getSelectedItem();
        if (dic != null) {
            FileWorker.rewriteDic(dic, (ArrayList<String>) Client.getDicFromServer(dic));
            internalDicsListView.setItems(FXCollections.observableArrayList(FileWorker.getDicNames()));
        }
    }

}
