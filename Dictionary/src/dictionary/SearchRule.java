/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import java.util.ArrayList;

/**
 *
 * @author Евгений
 */
public class SearchRule {

    //The name of the dictionary
    public String dicName;
    //Splitter of terms
    public String splitter;
    public String excludeFrom;
    public String excludeTo;
    //position of definition in splitted string
    public ArrayList<Integer> defPos;

    public SearchRule(String dicN, String spl, int defP) {
        defPos = new ArrayList<>();
        dicName = dicN;
        splitter = spl;
        
        defPos.add(defP);
    }
}
