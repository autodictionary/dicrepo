/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

/**
 *
 * @author Evgeniy
 */
public class Term {

    public TermPart[] termParts;

    public Term(TermPart[] termParts) {
        this.termParts = termParts;
    }

    public String GetFullTerm() {
        String ret = "";
        for (TermPart tp : termParts) {
            ret += tp.actual + " ";
        }
        ret = ret.trim();
        return ret;
    }

    public String[] GetNormalParts() {
        int len = 0;
        for (TermPart tp : termParts) {
            if (!tp.ignored) {
                len++;
            }
        }
        String[] ret = new String[len];
        int j=0;
        for (int i = 0; i < termParts.length; i++) {
            if (!termParts[i].ignored) {
                ret[j] = termParts[i].normalized;
                j++;
            }
        }
        return ret;
    }

    public String[] GetCombinations() {
        String[] ret = new String[0];
        if (termParts.length > 2) {
            ret = new String[termParts.length - 1];
            for (int i = 0; i < termParts.length - 1; i++) {
                ret[i] = "";
                for (int j = 0; j < termParts.length; j++) {
                    if (j != i) {
                        ret[i] += termParts[j].actual + " ";
                    }
                }
                ret[i] = ret[i].trim();
            }
        }
        return ret;
    }
}
