/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import dictionaryserver.Server;
import dictionaryserver.ServerService;
import java.util.List;

/**
 *
 * @author Евгений
 */
public class Client {
    public static boolean saveDicOnServer(String dicName, List<String> dic)
    {
        ServerService service = new ServerService();
        Server server = service.getServerPort();
        return server.saveDic(dicName, dic);
    }
    
    public static List<String> getDicNamesFromServer()
    {
        ServerService service = new ServerService();
        Server server = service.getServerPort();
        return server.getDicNames();
    }
    
    public static List<String> getDicFromServer(String dicName)
    {
        ServerService service = new ServerService();
        Server server = service.getServerPort();
        return server.getDic(dicName);
    }
    
}
