/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

/**
 *
 * @author Евгений
 */

 enum SearchState{
    error,
    not_found,
    initial,
    external,
    online
}

public class ExitSearch {
        SearchState state;//-2-error;-1-not found;0-initial;1-external;2-online
        String definition;

        public ExitSearch(SearchState st, String def) {
            state = st;
            definition = def;
        }
}
