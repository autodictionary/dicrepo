/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Евгений Пиляев
 */
public class FileWorker {

    public static List<String> getDicNames() {
        List<String> fileNames = new ArrayList<String>();
        File folder = new File("Dictionaries/");
        File[] listOfFiles = folder.listFiles();
        for (File f : listOfFiles) {
            fileNames.add(f.getName());
        }
        return fileNames;
    }

    public static void write(String fileName, String text) {
        File file = new File("Dictionaries/"+fileName);

        try {

            if (!file.exists()) {
                file.createNewFile();
            }

            PrintWriter out = new PrintWriter(file.getAbsoluteFile());

            try {
                out.println(text);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ArrayList<String> read(String fileName) throws FileNotFoundException {

        ArrayList<String> list = new ArrayList<>();
        File file = new File("Dictionaries/"+fileName);
        if (!file.exists()) {
            return null;
        }

        try {

            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try {

                String s;
                while ((s = in.readLine()) != null) {
                    if (s.length() > 2) {
                        list.add(s);
                    }
                }
            } finally {

                in.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return list;
    }

    public static void rewriteDic(String fileName, ArrayList<String> text) {

        File file = new File("Dictionaries/"+fileName);

        try {
            file.delete();
            file.createNewFile();

            PrintWriter out = new PrintWriter(file.getAbsoluteFile());

            try {
                for (String text1 : text) {
                    out.println(text1);
                }
            } finally {

                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
