/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

import static dictionary.Dictionary.driver;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Евгений
 */
public class WebDictionary {
    //доделать многостраничный поиск

    public static ExitSearch findWordInWebVedu(String word) {

        try {
            driver = new ChromeDriver();
            String exitString = "";
            String[] urls;
            driver.navigate().to("http://www.vedu.ru/expdic/");

            driver.findElement(By.id("Enc_String")).sendKeys(word);
            driver.findElement(By.id("SearchButton")).click();
            
             if (!driver.getPageSource().contains("result-summary")) {
                driver.close();
                driver.quit();
                return new ExitSearch(SearchState.not_found, null);
            }
            
            String[] response = driver.findElement(By.className("result-summary")).getText().split(":");
            int wordsCount = Integer.parseInt(response[response.length - 1]);

            urls = new String[wordsCount];

            for (int i = 0; i < wordsCount; i++) {
                try {
                    int n = i + 1;
                    urls[i] = driver.findElement(By.xpath("//table[@class='center']/tbody/tr[" + n + "]/td/a")).getAttribute("href");
                } catch (Exception ex) {
                }
            }
            for (int i = 0; i < wordsCount; i++) {
                try {
                    driver.navigate().to(urls[i]);
                    exitString += driver.findElement(By.id("acontent")).getText() + "\n\n";
                } catch (Exception ex) {
                }
            }
            driver.close();
            driver.quit();
            return new ExitSearch(SearchState.online, exitString);
        } catch (Exception ex) {
            System.err.println(ex);
            try {
                driver.close();
                driver.quit();
            } catch (Exception e) {
            }

            return new ExitSearch(SearchState.error, null);
        }
    }

    public static ExitSearch findWordInWebSumIn(String word) {
        try {
            driver = new ChromeDriver();
            String exitString = "";
            String[] urls;
            driver.navigate().to("http://sum.in.ua/");

            driver.findElement(By.id("query")).sendKeys(word);
            driver.findElement(By.id("search")).click();
            if (driver.getPageSource().contains("не знайдено")) {
                driver.close();
                driver.quit();
                return new ExitSearch(SearchState.not_found, null);
            } else {
                exitString += driver.findElement(By.xpath("//div[@itemprop='articleBody']")).getText();
            }
            driver.close();
            driver.quit();
            return new ExitSearch(SearchState.online, exitString);
        } catch (Exception ex) {
            System.err.println(ex);
            try {
                driver.close();
                driver.quit();
            } catch (Exception e) {
            }
            return new ExitSearch(SearchState.error, null);
        }
    }

    public static ExitSearch findWordInWebSlovopedia(String word) {
        try {
            driver = new ChromeDriver();
            String exitString = "";
            String[] urls;
            driver.navigate().to("http://www.slovopedia.com/");

            driver.findElement(By.name("query")).sendKeys(word);
            driver.findElement(By.name("submit")).click();

            if (driver.getPageSource().contains("По вашему запросу ничего не найдено. ")) {
                driver.close();
                driver.quit();
                return new ExitSearch(SearchState.not_found, null);
            }

            WebElement wordsHost = driver.findElements(By.xpath("//div[@class='sclear']")).get(1);
            List<WebElement> words = wordsHost.findElements(By.tagName("a"));
            urls = new String[words.size()];

            for (int i = 0; i < words.size(); i++) {
                int n = i + 1;
                urls[i] = words.get(i).getAttribute("href");
            }
            for (int i = 0; i < words.size(); i++) {
                driver.navigate().to(urls[i]);
                exitString += driver.findElement(By.xpath("//div[@class='word']")).getText() + "\n\n";
            }
            driver.close();
            driver.quit();
            return new ExitSearch(SearchState.online, exitString);
        } catch (Exception ex) {
            System.err.println(ex);
            try {
                driver.close();
                driver.quit();
            } catch (Exception e) {
            }
            return new ExitSearch(SearchState.error, null);
        }
    }
}
