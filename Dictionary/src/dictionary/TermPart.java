/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dictionary;

/**
 *
 * @author Evgeniy
 */
public class TermPart {
    public String actual;
    public String normalized;
    public boolean ignored = false;
    
    public TermPart(String actual, String normalized){
        this.actual = actual;
        this.normalized = normalized;
    }
    
    public TermPart(String actual, String normalized, boolean ignored){
        this.actual = actual;
        this.normalized = normalized;
        this.ignored = ignored;
    }
}
