
package dictionaryserver;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dictionaryserver package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetDicNamesResponse_QNAME = new QName("http://dictionaryserver/", "GetDicNamesResponse");
    private final static QName _SaveDicResponse_QNAME = new QName("http://dictionaryserver/", "SaveDicResponse");
    private final static QName _SaveDic_QNAME = new QName("http://dictionaryserver/", "SaveDic");
    private final static QName _GetDic_QNAME = new QName("http://dictionaryserver/", "GetDic");
    private final static QName _GetDicResponse_QNAME = new QName("http://dictionaryserver/", "GetDicResponse");
    private final static QName _GetDicNames_QNAME = new QName("http://dictionaryserver/", "GetDicNames");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dictionaryserver
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetDicNames }
     * 
     */
    public GetDicNames createGetDicNames() {
        return new GetDicNames();
    }

    /**
     * Create an instance of {@link GetDicResponse }
     * 
     */
    public GetDicResponse createGetDicResponse() {
        return new GetDicResponse();
    }

    /**
     * Create an instance of {@link GetDic }
     * 
     */
    public GetDic createGetDic() {
        return new GetDic();
    }

    /**
     * Create an instance of {@link SaveDic }
     * 
     */
    public SaveDic createSaveDic() {
        return new SaveDic();
    }

    /**
     * Create an instance of {@link SaveDicResponse }
     * 
     */
    public SaveDicResponse createSaveDicResponse() {
        return new SaveDicResponse();
    }

    /**
     * Create an instance of {@link GetDicNamesResponse }
     * 
     */
    public GetDicNamesResponse createGetDicNamesResponse() {
        return new GetDicNamesResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDicNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "GetDicNamesResponse")
    public JAXBElement<GetDicNamesResponse> createGetDicNamesResponse(GetDicNamesResponse value) {
        return new JAXBElement<GetDicNamesResponse>(_GetDicNamesResponse_QNAME, GetDicNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDicResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "SaveDicResponse")
    public JAXBElement<SaveDicResponse> createSaveDicResponse(SaveDicResponse value) {
        return new JAXBElement<SaveDicResponse>(_SaveDicResponse_QNAME, SaveDicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDic }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "SaveDic")
    public JAXBElement<SaveDic> createSaveDic(SaveDic value) {
        return new JAXBElement<SaveDic>(_SaveDic_QNAME, SaveDic.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDic }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "GetDic")
    public JAXBElement<GetDic> createGetDic(GetDic value) {
        return new JAXBElement<GetDic>(_GetDic_QNAME, GetDic.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDicResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "GetDicResponse")
    public JAXBElement<GetDicResponse> createGetDicResponse(GetDicResponse value) {
        return new JAXBElement<GetDicResponse>(_GetDicResponse_QNAME, GetDicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDicNames }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dictionaryserver/", name = "GetDicNames")
    public JAXBElement<GetDicNames> createGetDicNames(GetDicNames value) {
        return new JAXBElement<GetDicNames>(_GetDicNames_QNAME, GetDicNames.class, null, value);
    }

}
